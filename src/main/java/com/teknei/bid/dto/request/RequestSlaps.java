package com.teknei.bid.dto.request;

import com.teknei.bid.dto.Fingers;
import com.teknei.bid.dto.Slaps;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Amaro on 06/07/2017.
 */
@Data
public class RequestSlaps implements Serializable {

    private String id;
    private Slaps slaps;

}
