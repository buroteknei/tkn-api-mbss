package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Amaro on 06/07/2017.
 */
@Data
public class Fingers implements Serializable {

    private String lt;
    private String li;
    private String lm;
    private String lr;
    private String ll;
    private String rt;
    private String ri;
    private String rm;
    private String rr;
    private String rl;

}
