package com.teknei.bid.ws.util;

import com.morpho.mbss.generic.wsdl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.namespace.QName;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Amaro on 07/07/2017.
 */
@Component
public class WSInvoker {

    @Value("${mbss.wsdl}")
    private String wsdl;
    @Value("${mbss.qname.url}")
    private String qnameURL;
    @Value("${mbss.qname.name}")
    private String qnameServiceName;
    private IWSMbss mbss;
    private RoutingData routingData;
    private static final Logger log = LoggerFactory.getLogger(WSInvoker.class);

    @PostConstruct
    private void postConstruct() {
        buildPortType();
    }

    private void buildPortType() 
    {
    	//log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".buildPorType http::\\morpho.com+++++++++++++++ ");
        try {
            QName qname = new QName("http://www.morpho.com/mbss/generic/wsdl", "MbssService");
            this.routingData = new RoutingData();
            this.routingData.setPriority(Integer.valueOf(5));
            this.routingData.setMatchToPersonSLALevel(Integer.valueOf(0));
            URL url = new URL(wsdl);
            MbssService service = new MbssService(url, qname);
            this.mbss = service.getMbssPort();
        } catch (MalformedURLException e) {
            log.error("Error building WSDL: {}", e.getMessage());
        }
    }

    public Response     doRequest(Request request) {
    	//log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".doRequest ");
        Response response = null;
        if (this.mbss != null) {
            try {
                if (request != null) {
                    if (mbss == null) {
                        buildPortType();
                    }
                    response = this.mbss.process(request, this.routingData);
                }
            } catch (Exception e) {
                log.error("Error requesting data to mbss server: {}", e.getMessage());
            }
        }

        return response;
    }

}
