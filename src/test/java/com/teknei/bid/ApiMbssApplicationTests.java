package com.teknei.bid;

import com.teknei.bid.controller.rest.MBSSController;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class ApiMbssApplicationTests {

    //@Autowired
    private MBSSController controller;
    private static final Logger log = LoggerFactory.getLogger(ApiMbssApplicationTests.class);

    //@Test
    public void contextLoads() throws Exception {

        ResponseEntity<String> responseEntity1 = controller.getHashForPerson("2");
        JSONObject jsonObject1 = new JSONObject(responseEntity1.getBody());
        log.info("Response 1 : {}", jsonObject1);
        ResponseEntity<String> responseEntity2 = controller.getHashForPerson("1992");
        JSONObject jsonObject2 = new JSONObject(responseEntity2.getBody());
        log.info("Response 2: {}", jsonObject2);
        assertEquals(jsonObject1.toString(), jsonObject2.toString());
        //ResponseEntity<String> responseEntity3 = controller.getHashForPerson("45");
        //JSONObject jsonObject3 = new JSONObject(responseEntity3.getBody());
        //log.info("Response 3: {}", jsonObject3);
        //assertNotEquals(jsonObject1.toString(), jsonObject3.toString());
    }

}
